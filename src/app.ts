import express from 'express'
import mongoose from 'mongoose'
import projectRouter from './routes/project/Project.route'
import userRouter from './routes/user/User.route'
import authRouter from './routes/Auth'
import bodyParser from 'body-parser'
import helmet from 'helmet'
import cors from 'cors'


const app = express()

mongoose
    .connect('mongodb+srv://Taras:Zaebars1488@cluster0.rcrmu.mongodb.net/projects?retryWrites=true&w=majority', {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true,
    })
    .then(() => {
    })
    .catch((err: Error) => {
        console.log(
            'MongoDB connection error. Please make sure MongoDB is running. ' + err
        )
    })

app.set('port', 8000)

app.use(helmet())
app.use(cors())
app.use(bodyParser.json())

app.use('/api/v1/project', projectRouter)
app.use('/api/v1/user', userRouter)
app.use('/api/v1/auth', authRouter)

export default app


//see excel file
//ask about data set examples