import mongoose, { Document, Schema } from 'mongoose'
import { ProjectDocument } from '../project/Project.model'

export type UserProjectsDocument = Document & {
    projects: ProjectDocument[];
}

export const UserProjectsSchema = new mongoose.Schema({
    projects: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Project' }],
})

export default mongoose.model<UserProjectsDocument>('UserProjects', UserProjectsSchema)