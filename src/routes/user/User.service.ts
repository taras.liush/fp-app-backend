import { UserProjectsDocument } from '../userProjects/UserProjects.model'
import User, { UserDocument } from './User.model'

function create(user: UserDocument): Promise<UserDocument> {
    return user.save()
}

async function find(id: string): Promise<UserDocument> {
    const user = await User.findById(id).populate('projects').exec()
    if (!user) {
        throw new Error(`User ${id} not found`)
    }
    console.log(user)
    return user
}

function createProject(
    id: string,
    project: UserProjectsDocument
): Promise<UserDocument> {
    return User.findById(id)
        .exec()
        .then((user) => {
            if (!user) {
                throw new Error(`User ${id} not found`)
            }
            user.projects = user.projects.concat(project)
            return user.save()
        })
}

export default {
    create,
    find,
    createProject,
}
