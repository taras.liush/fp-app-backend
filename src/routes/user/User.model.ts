import mongoose, { Document, Schema } from 'mongoose'
import UserProjects, { UserProjectsDocument, UserProjectsSchema } from '../userProjects/UserProjects.model'

export type UserDocument = Document & {
    username?: string;
    email: string;
    googleId: string;
    projects: UserProjectsDocument[];
}

const userSchema = new mongoose.Schema({
    username: { type: String, default: '' },
    email: String,
    googleId: String,
    projects: [{ type: Schema.Types.ObjectId, ref: 'Project' }],
})

export default mongoose.model<UserDocument>('User', userSchema)