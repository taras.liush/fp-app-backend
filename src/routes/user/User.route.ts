import express from 'express'
import {
    createUser,
    findUser,
    createProject
} from './User.controller'

const router = express.Router()

router.post('/signup', createUser)
router.get('/user', findUser)
router.post('/user', createProject)

export default router