import { Request, Response, NextFunction } from 'express'
import User from './User.model'
import userService from './User.service'
import jwt from 'jsonwebtoken'


export const createUser = async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    const user = new User(req.body)
    await userService.create(user)
    res.json(user)
}

export const createProject = async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    const token = req.headers.authorization?.split(' ')[1]
    const userId: any = jwt.verify(token!, 'privatekey')
    //@ts-ignore
    const project = req.body
    const createdProject = await userService.createProject(userId, project)
    res.json(createdProject)

}

export const findUser = async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    const token = req.headers.authorization?.split(' ')[1]
    console.log(token)
    const userToken = jwt.verify(token!, 'privatekey')
    //@ts-ignore
    const user = await userService.find(userToken.id)
    res.json(user.projects)
}









