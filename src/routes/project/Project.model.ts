import mongoose, { Document, Schema } from 'mongoose'

export type ProjectDocument = Document & {
    userId: string;
    settings: object;
}

const ProjectSchema = new mongoose.Schema({
    userId: String,
    settings: Object,
})

export default mongoose.model<ProjectDocument>('Project', ProjectSchema)