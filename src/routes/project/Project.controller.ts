import { Request, Response, NextFunction } from 'express'
import ProjectService from './Project.service'
import Project from './Project.model'
import jwt from 'jsonwebtoken'

export const findAll = async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    const token = req.headers.authorization?.split(' ')[1]
    const user = jwt.verify(token!, 'privatekey')
    res.json(await ProjectService.findAll())
}

export const findById = async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    res.json(await ProjectService.findById(req.params.projectId))
}

export const createProject = async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    const token = req.headers.authorization?.split(' ')[1]
    const userToken = jwt.verify(token!, 'privatekey')
    //@ts-ignore
    const project = new Project({ settings: req.body, userId: userToken.id })
    console.log(req.body)
    const newProject = await ProjectService.create(project)
    res.json(newProject)
}

export const updateProject = async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    const update = req.body
    const projectId = req.params.projectId
    const updateProject = await ProjectService.update(projectId, update)
    res.json(updateProject)
}

export const deleteProject = async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    await ProjectService.deleteProject(req.params.projectId)
    res.status(204).end()
}